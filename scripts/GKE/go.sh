#!/usr/bin/env bash

# This script aims to be a 1 click deployment of jx up to but not including
# the importing of a repository.

source vars.sh # Get vars for command


jx create cluster gke \
	--batch-mode \
	--skip-installation \
	--cluster-name "${cluster_name}" \
	--default-environment-prefix "${environment_prefix}" \
	--disk-size "${disk_size}" \
	--git-api-token "${git_api_token}" \
	--git-private \
	--git-username "${git_username}" \
	--install-dependencies \
	--kaniko \
	--machine-type "${machine_type}" \
	--max-num-nodes "${max_num_nodes}" \
	--min-num-nodes "${min_number_nodes}" \
	--project-id "${project_id}" \
	--skip-login \
	--zone "${zone}" \
