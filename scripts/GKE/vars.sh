#!/usr/bin/env bash

cluster_name='marjorie'
environment_prefix="${cluster_name}"
disk_size='25'
git_api_token='455f310098e251092b8bcee90147878286d49147'
git_username='alphabettispaghetti'
machine_type='custom-2-4096' # Specify custom machines like this: "custom-{cpus}-{MiB-ram}"
max_num_nodes='5'
min_num_nodes='3'
project_id='imarealfungi-1551974438618'
zone='europe-west2-a'
