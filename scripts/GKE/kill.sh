#!/usr/bin/env bash


source vars.sh # Get vars for command

gcloud container clusters delete "${cluster_name}" \
	--batch-mode \
	--project "${project_id}" \
	--zone "${zone}"
