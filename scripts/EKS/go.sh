#!/usr/bin/env bash

# This script aims to be a 1 click deployment of jx up to but not including
# the importing of a repository.

source ./vars.sh

# Get credentials for script
export AWS_ACCESS_KEY_ID=$(grep -i 'aws_access_key' ~/.aws/credentials | awk '{print $NF}')
export AWS_SECRET_ACCESS_KEY=$(grep -i 'aws_secret_access_key' ~/.aws/credentials | awk '{print $NF}')

jx create cluster eks \
	--batch-mode \
	--cluster-name "${cluster_name}" \
	--default-environment-prefix "${cluster_name}" \
 	--git-api-token ${git_api_token} \
 	--git-private \
 	--git-username ${git_username} \
 	--node-type ${node_type} \
 	--nodes-max ${nodes_max} \
 	--nodes-min ${nodes_min} \
	--region ${region} \
	--tags ${tags} \
	--zones ${zones}
