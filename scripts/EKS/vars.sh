#!/usr/bin/env bash

cluster_name='cubana'
region='eu-west-1'
git_api_token='455f310098e251092b8bcee90147878286d49147'
git_username='alphabettispaghetti'
node_type='t2.medium'
nodes_max='5'
nodes_min='1'
tags="Name=Adam,Cluster=${cluster_name}"
zones="${region}a,${region}b,${region}c"
