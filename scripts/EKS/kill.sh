#!/usr/bin/env bash

source vars.sh

# THIS ISN'T WORKING RIGHT NOW, I WONDER IF A BETTER WAY WOULD BE TO KILL THE ELB AND THEN THE
# TWO CLOUDFORMATION STACKS

# Need to delete internet gateway
# internet_gateway_to_delete=\
# 	$(aws ec2 describe-internet-gateways \
# 		--region "${region}" \
# 		--filters Name=tag:Name,Values="eksctl-${cluster_name}-cluster/InternetGateway" \
# 	| jq -r '.InternetGateways[].InternetGatewayId')

vpc_to_delete=\
	$(aws ec2 describe-vpcs \
		--region "${region}" \
		--filters Name=tag:Name,Values="eksctl-${cluster_name}-cluster/VPC" \
	| jq -r '.Vpcs[].VpcId')

# aws ec2 detach-internet-gateway \
# 	--internet-gateway-id ${internet_gateway_to_delete} \
# 	--vpc-id ${vpc_to_delete}

jx delete aws \
	--vpc-id ${vpc_to_delete} \
	--region "${region}"

# jx delete eks \
# 	--output 'yaml' \
# 	--profile 'default' \
# 	--region ${region}
