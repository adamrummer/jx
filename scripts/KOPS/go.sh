#!/usr/bin/env bash

# This script aims to be a 1 click deployment of jx up to but not including
# the importing of a repository.


# Get credentials for script
export AWS_ACCESS_KEY_ID=$(grep -i 'aws_access_key' ~/.aws/credentials | awk '{print $NF}')
export AWS_SECRET_ACCESS_KEY=$(grep -i 'aws_secret_access_key' ~/.aws/credentials | awk '{print $NF}')


# SET UP

# Checks existance of bucket for state files, creates unique bucket if it isn't accessible
echo "Checking that the S3 bucket ${state_bucket} exists."
existance_check=$(aws s3api head-bucket --bucket "${state_bucket}")
if [[ -z ${existance_check} ]]; then
	echo "The S3 bucket ${state_bucket} exists."
	echo "Checking contents of the S3 bucket."
	# Checks if bucket is empty
	empty_check=$(aws s3 ls s3://"${state_bucket}")
	if [[ ! -z ${empty_check} ]]; then
		echo "NOTE: the bucket ${state_bucket} was not empty. Deleting contents now."
		aws s3 rm s3://"${state_bucket}" --recursive
	fi
else
	echo "The S3 bucket ${state_bucket} either does not exist or you do not have permission to view it."
	state_bucket="${cluster_name}-kubernetes-state_bucket"
	echo "Creating a new bucket with (hopefully) unique name: ${state_bucket}"
	aws s3 create-bucket --bucket "${state_bucket}"
	echo "Bucket ${state_bucket} successfully created, storing state there."
fi

# Creates cluster
echo "Attempting to make a kubernetes cluster with Jenkins X on AWS with KOPS..."
jx create cluster aws \
	--batch-mode \
	--verbose \
	--cluster-name "${cluster_name}" \
	--default-environment-prefix "${cluster_name}" \
 	--git-api-token '455f310098e251092b8bcee90147878286d49147' \
 	--git-private \
 	--git-username 'alphabettispaghetti' \
	--ingress-deployment 'jx-ingress-controller' \
	--ingress-service 'jx-ingress-service'\
 	--node-size 't2.medium' \
 	--nodes '3' \
	--master-size 't2.large' \
 	--state "s3://${state_bucket}" \
	--region "${region}" \
	--zones "${region}a,${region}b,${region}c"
