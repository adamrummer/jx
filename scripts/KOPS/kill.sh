#!/usr/bin/env bash

source ./vars.sh


kops delete cluster --name "${cluster_name}.cluster.k8s.local" --yes
